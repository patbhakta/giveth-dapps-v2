import { TabOverview } from '../homeTabs/Overview';

import Tabs from '../Tabs';

function HomeView() {
	return (
		<>
			<Tabs />
			<TabOverview />
		</>
	);
}

export default HomeView;
